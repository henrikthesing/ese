<?php

declare(strict_types=1);

namespace Administration\Controller;

use Application\Service\ClientService;
use Application\Service\UserService;
use Laminas\Authentication\AuthenticationService;
use Laminas\Crypt\Password\Bcrypt;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Storage\Entity\User;

class IdentityController extends AbstractAuthenticationController
{
    public function __construct(
        AuthenticationService $authenticationService,
        UserService $userService,
        ClientService $clientService
    ) {
        parent::__construct(
            $authenticationService,
            $userService,
            $clientService
        );
    }

    /**
     * @return Response|ViewModel
     */
    public function indexAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();

        assert($this->identity instanceof User);
        $user = $this->userService->find((int) $this->identity->getId());
        if ($user === null) {
            return $this->createViewModel();
        }

        if ($request->isPost()) {
            $formData = $request->getPost('data');

            if ($formData['password'] !== $formData['passwordrep']) {
                return $this->createViewModel();
            }

            $bcrypt = new Bcrypt();
            $passwordHash = $bcrypt->create($formData['password']);

            if (!empty($formData['password'])) {
                $user->setPassword($passwordHash);
            }

            $this->userService->save($user);

            return $this->createViewModel([
                'successMessage' => 'Das Passwort wurde erfolgreich geändert.',
            ]);
        }

        return $this->createViewModel();
    }
}