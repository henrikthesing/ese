<?php

declare(strict_types=1);

namespace Administration\Controller;

use Application\Service\ClientService;
use Application\Service\ContactService;
use Application\Service\FacilityService;
use Application\Service\TagGroupService;
use Application\Service\TagService;
use Application\Service\UserService;
use Laminas\Authentication\AuthenticationService;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Storage\Entity\Contact;
use Storage\Entity\Tag;
use Storage\Entity\TagGroup;

class AdministrationController extends AbstractAuthenticationController
{
    private TagGroupService $tagGroupService;

    private TagService $tagService;

    private FacilityService $facilityService;

    private ContactService $contactService;

    public function __construct(
        AuthenticationService $authenticationService,
        UserService $userService,
        ClientService $clientService,
        TagGroupService $tagGroupService,
        TagService $tagService,
        FacilityService $facilityService,
        ContactService $contactService
    ) {
        parent::__construct($authenticationService, $userService, $clientService);

        $this->tagGroupService = $tagGroupService;
        $this->tagService = $tagService;
        $this->facilityService = $facilityService;
        $this->contactService = $contactService;
    }

    /**
     * @return Response|ViewModel
     */
    public function indexAction()
    {
        return $this->redirect()->toRoute('administration/facilities', [], [], true);
    }

    //--------------------------------------------------------------------------------------------------
    // Contacts
    //--------------------------------------------------------------------------------------------------

    public function contactsAction(): ViewModel
    {
        $facilityId = $this->params()->fromRoute('facilityId');
        $facility = $this->facilityService->getFacility((int) $facilityId, $this->selectedClient);

        return $this->createViewModel([
            'facility' => $facility
        ]);
    }

    /**
     * @return ViewModel|Response
     */
    public function contactsAddAction()
    {
        $facilityId = $this->params()->fromRoute('facilityId');
        $facility = $this->facilityService->getFacility((int) $facilityId, $this->selectedClient);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $formData = $request->getPost('data');

            $contact = new Contact($facility);
            $contact
                ->setFirstname($formData['firstname'])
                ->setLastname($formData['lastname'])
                ->setPhoneNumber($formData['phone'])
                ->setMobilenumber($formData['mobile'])
                ->setFaxnumber($formData['fax'])
                ->setEmail($formData['email'])
                ->setPosition($formData['position']);
            $facility->addContact($contact);

            $this->facilityService->save($facility);

            $this->redirect()->toRoute('administration/facilities/contacts', [
                'facilityId' => $facility->getId(),
            ], [], true);
        }

        return $this->createViewModel([
            'facility' => $facility,
        ]);
    }

    /**
     * @return ViewModel|Response
     */
    public function contactsEditAction()
    {
        $facilityId = $this->params()->fromRoute('facilityId');
        $facility = $this->facilityService->getFacility((int) $facilityId, $this->selectedClient);

        $contactId = $this->params()->fromRoute('contactId');
        $contact = $this->contactService->getContact((int) $contactId);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $formData = $request->getPost('data');

            $contact
                ->setFirstname($formData['firstname'])
                ->setLastname($formData['lastname'])
                ->setPhoneNumber($formData['phone'])
                ->setMobilenumber($formData['mobile'])
                ->setFaxnumber($formData['fax'])
                ->setEmail($formData['email'])
                ->setPosition($formData['position'])
                ->setUpdatedAt(new \DateTime('now', new \DateTimeZone('utc')));

            $this->contactService->save($contact);

            $this->redirect()->toRoute('administration/facilities/contacts', [
                'facilityId' => $facility->getId(),
            ], [], true);
        }

        return $this->createViewModel([
            'contact' => $contact,
            'facility' => $facility,
        ]);
    }

    public function contactsDeleteAction(): Response
    {
        $facilityId = $this->params()->fromRoute('facilityId');
        $facility = $this->facilityService->getFacility((int) $facilityId, $this->selectedClient);

        $contactId = $this->params()->fromRoute('contactId');
        $contact = $this->contactService->getContact((int) $contactId);

        $this->contactService->delete($contact);

        $this->redirect()->toRoute('administration/facilities/contacts', [
            'facilityId' => $facility->getId(),
        ], [], true);
    }

    //--------------------------------------------------------------------------------------------------
    // TagGroups
    //--------------------------------------------------------------------------------------------------

    public function taggroupsAction(): ViewModel
    {
        $tagGroups = $this->tagGroupService->getTagGroups($this->selectedClient);

        return $this->createViewModel([
            'tagGroups' => $tagGroups,
        ]);
    }

    /**
     * @return ViewModel|Response
     */
    public function taggroupsAddAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            /** @var TagGroup $tagGroup */
            $tagGroup = new TagGroup($this->selectedClient);

            $formData = $request->getPost('data');
            $tagGroup->setName($formData['name']);

            $this->tagGroupService->save($tagGroup);

            $this->redirect()->toRoute('administration/taggroups', [], [], true);
        }

        return $this->createViewModel();
    }

    /**
     * @return ViewModel|Response
     */
    public function taggroupsEditAction()
    {
        $tagGroupId = $this->params()->fromRoute('tagGroupId');

        /** @var TagGroup $tagGroups */
        $tagGroup = $this->tagGroupService->getTagGroup((int) $tagGroupId);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $formData = $request->getPost('data');
            $tagGroup->setName($formData['name']);

            $this->tagGroupService->save($tagGroup);

            $this->redirect()->toRoute(
                'administration/taggroups',
                [
                    'tagGroupId' => $tagGroup->getId(),
                ], [], true
            );
        }

        return $this->createViewModel([
            'tagGroup' => $tagGroup,
        ]);
    }

    public function taggroupsDeleteAction(): Response
    {
        $tagGroupId = $this->params()->fromRoute('tagGroupId');

        /** @var TagGroup $tagGroups */
        $tagGroup = $this->tagGroupService->getTagGroup((int) $tagGroupId);

        $this->tagGroupService->delete($tagGroup);

        $this->redirect()->toRoute('administration/taggroups', [], [], true);
    }

    //--------------------------------------------------------------------------------------------------
    // Tags
    //--------------------------------------------------------------------------------------------------

    public function tagsAction(): ViewModel
    {
        $tagGroupId = $this->params()->fromRoute('tagGroupId');

        /** @var TagGroup $tagGroups */
        $tagGroup = $this->tagGroupService->getTagGroup((int) $tagGroupId);

        return $this->createViewModel([
            'tagGroup' => $tagGroup,
        ]);
    }

    /**
     * @return ViewModel|Response
     */
    public function tagsAddAction()
    {
        $tagGroupId = $this->params()->fromRoute('tagGroupId');

        /** @var TagGroup $tagGroups */
        $tagGroup = $this->tagGroupService->getTagGroup((int) $tagGroupId);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $tag = new Tag();

            $formData = $request->getPost('data');
            $tag->setName($formData['name']);
            $tag->setTagGroup($tagGroup);

            $this->tagService->save($tag);

            $this->redirect()->toRoute(
                'administration/taggroups/tags',
                [
                    'tagGroupId' => $tagGroup->getId(),
                ], [], true
            );
        }

        return $this->createViewModel([
            'tagGroup' => $tagGroup
        ]);
    }

    /**
     * @return ViewModel|Response
     */
    public function tagsEditAction()
    {
        $tagGroupId = $this->params()->fromRoute('tagGroupId');
        $tagId = $this->params()->fromRoute('tagId');

        /** @var TagGroup $tagGroups */
        $tagGroup = $this->tagGroupService->getTagGroup((int) $tagGroupId);
        $tag = $this->tagService->getTag((int) $tagId);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $formData = $request->getPost('data');
            $tag->setName($formData['name']);

            $this->tagService->save($tag);

            $this->redirect()->toRoute(
                'administration/taggroups/tags',
                [
                    'tagGroupId' => $tagGroup->getId(),
                ], [], true
            );
        }

        return $this->createViewModel([
            'tagGroup' => $tagGroup,
            'tag' => $tag,
        ]);
    }

    public function tagsDeleteAction(): Response
    {
        $tagGroupId = $this->params()->fromRoute('tagGroupId');
        $tagId = $this->params()->fromRoute('tagId');

        /** @var TagGroup $tagGroups */
        $tagGroup = $this->tagGroupService->getTagGroup((int) $tagGroupId);
        $tag = $this->tagService->getTag((int) $tagId);

        $this->tagService->delete($tag);

        $this->redirect()->toRoute('administration/taggroups/tags', [
            'tagGroupId' => $tagGroup->getId()
        ], [], true);
    }
}
