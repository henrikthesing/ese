<?php

declare(strict_types=1);

namespace Storage\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200412083931 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE facility_tags (facility_id INT UNSIGNED NOT NULL, tag_id INT UNSIGNED NOT NULL, INDEX IDX_4313CED4A7014910 (facility_id), UNIQUE INDEX UNIQ_4313CED4BAD26311 (tag_id), PRIMARY KEY(facility_id, tag_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE facility_tags ADD CONSTRAINT FK_4313CED4A7014910 FOREIGN KEY (facility_id) REFERENCES facilities (id)');
        $this->addSql('ALTER TABLE facility_tags ADD CONSTRAINT FK_4313CED4BAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id)');
        $this->addSql('ALTER TABLE tags ADD taggroup_id INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE tags ADD CONSTRAINT FK_6FBC9426B82D1590 FOREIGN KEY (taggroup_id) REFERENCES taggroups (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6FBC9426B82D1590 ON tags (taggroup_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE facility_tags');
        $this->addSql('ALTER TABLE tags DROP FOREIGN KEY FK_6FBC9426B82D1590');
        $this->addSql('DROP INDEX UNIQ_6FBC9426B82D1590 ON tags');
        $this->addSql('ALTER TABLE tags DROP taggroup_id');
    }
}
