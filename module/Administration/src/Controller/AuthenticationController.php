<?php

declare(strict_types=1);

namespace Administration\Controller;

use Administration\Service\AuthManagerService;
use Application\Service\UserService;
use Exception;
use Laminas\Authentication\AuthenticationService;
use Laminas\Authentication\Result;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Storage\Entity\User;

class AuthenticationController extends AbstractActionController
{
    private AuthManagerService $authManagerService;

    private AuthenticationService $authService;

    private UserService $userService;

    public function __construct(
        AuthManagerService $authManagerService,
        AuthenticationService $authService,
        UserService $userService
    ) {
        $this->authManagerService = $authManagerService;
        $this->authService = $authService;
        $this->userService = $userService;
    }

    public function authenticateAction(): Response
    {
        if ($this->authService->hasIdentity()) {
            return $this->redirect()->toRoute('administration', [
                'clientId' => $this->getClientId()
            ]);
        }

        return $this->redirect()->toRoute('authentication/login');
    }

    /**
     * @return Response|ViewModel
     */
    public function loginAction()
    {
        $layout = $this->layout();
        $layout->setTemplate('layout/login');

        $isLoginError = false;

        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $email = $request->getPost('mailaddress');
            $password = $request->getPost('password');
            $rememberMe = (bool) $request->getPost('rememberMe');

            $result = $this->authManagerService->login($email, $password, $rememberMe);
            if ($result->getCode() === Result::SUCCESS) {
                return $this->redirect()->toRoute('administration', [
                    'clientId' => $this->getClientId()
                ]);
            } else {
                $isLoginError = true;
            }
        }

        return new ViewModel([
            'isLoginError' => $isLoginError
        ]);
    }

    /**
     * @throws Exception
     */
    public function logoutAction(): Response
    {
        try {
            $this->authManagerService->logout();
        } catch (Exception $exception) {
        }

        return $this->redirect()->toRoute('authentication');
    }

    private function getClientId(): int
    {
        $user = $this->userService->findByEmail($this->authService->getIdentity());
        assert($user instanceof User);

        $clientId = $user->getClient()->getId();

        if (!is_int($clientId)) {
            throw new \InvalidArgumentException(
                'Invalid argument given for selected client id',
                1298763891234
            );
        }

        return $clientId;
    }
}
