<?php

declare(strict_types=1);

namespace Storage\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211001213123 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Adds new table for clients';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE clients (id INT UNSIGNED AUTO_INCREMENT NOT NULL, client_key VARCHAR(40) NOT NULL, name VARCHAR(200) NOT NULL, UNIQUE INDEX uq_client_key (client_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
