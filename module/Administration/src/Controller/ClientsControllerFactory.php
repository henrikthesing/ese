<?php

declare(strict_types=1);

namespace Administration\Controller;

use Application\Service\ClientService;
use Application\Service\UserService;
use Laminas\Authentication\AuthenticationService;
use Psr\Container\ContainerInterface;

class ClientsControllerFactory
{
    public function __invoke(ContainerInterface $container): ClientsController
    {
        return new ClientsController(
            $container->get(AuthenticationService::class),
            $container->get(UserService::class),
            $container->get(ClientService::class)
        );
    }
}