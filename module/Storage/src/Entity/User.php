<?php

declare(strict_types=1);

namespace Storage\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="users",
 *     options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"}
 * )
 * @ORM\Entity(repositoryClass="Storage\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Column(name="id", type="integer", options={"unsigned": true})
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\OneToOne(targetEntity="UserRole")
     * @ORM\JoinColumn(name="userrole_id", referencedColumnName="id")
     */
    private UserRole $role;

    /**
     * @ORM\OneToOne(targetEntity="Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private Client $client;

    /**
     * @ORM\Column(name="email", type="string", length=100, unique=true)
     */
    private string $email;

    /**
     * @ORM\Column(name="password", type="string", length=60, nullable=true)
     */
    private ?string $password;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private DateTime $createdAt;

    /**
     * @ORM\Column(name="activated_at", type="datetime", nullable=true)
     */
    private ?DateTime $activatedAt;

    public function __construct(string $email)
    {
        $this->email = $email;

        $this->createdAt = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getRole(): UserRole
    {
        return $this->role;
    }

    public function setRole(UserRole $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getActivatedAt(): ?DateTime
    {
        return $this->activatedAt;
    }

    public function setActivatedAt(DateTime $activatedAt): self
    {
        $this->activatedAt = $activatedAt;

        return $this;
    }

    public function isActive(): bool
    {
        return (null !== $this->activatedAt) ?? true;
    }

    public function isAdministrator(): bool
    {
        return $this->role->getId() === UserRole::ROLE_ADMINISTRATOR;
    }

    public function isUser(): bool
    {
        return $this->role->getId() === UserRole::ROLE_USER;
    }
}
