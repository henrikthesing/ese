<?php

declare(strict_types=1);

namespace Application\Service;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Storage\Entity\Client;
use Storage\Repository\ClientRepository;

class ClientServiceFactory
{
    public function __invoke(ContainerInterface $container): ClientService
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);
        /** @var ClientRepository $clientRepository */
        $clientRepository = $entityManager->getRepository(Client::class);

        return new ClientService(
            $clientRepository
        );
    }
}
