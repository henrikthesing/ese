<?php

declare(strict_types=1);

namespace Application\Service;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Storage\Entity\Tag;
use Storage\Repository\TagRepository;

class TagServiceFactory
{
    public function __invoke(ContainerInterface $container): TagService
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);
        /** @var TagRepository $tagRepository */
        $tagRepository = $entityManager->getRepository(Tag::class);

        return new TagService(
            $tagRepository
        );
    }
}