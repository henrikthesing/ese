#!/usr/bin/env bash
echo "Fetching $1 branch"
git pull
git checkout $1
git reset HEAD --hard

echo "Updating composer dependencies"
/usr/bin/php7.4 composer.phar install --ignore-platform-reqs

echo "Migrating database"
/usr/bin/php7.4 public/index.php migrations:migrate -n
echo "Finished deployment"