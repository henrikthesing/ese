<?php

declare(strict_types=1);

namespace Administration\Logger;

use Laminas\Log\Logger as LaminasLogger;
use Laminas\Log\Writer\Stream;

class Logger extends LaminasLogger
{
	public function __construct($options = null)
	{
		parent::__construct($options);

		$writer = new Stream(
			sprintf(
				APP_DIR . '/../data/logs/%s.log',
				(new \DateTime())->format('Y-m-d')
			)
		);
		$this->addWriter($writer);
	}
}