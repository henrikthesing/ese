<?php

declare(strict_types=1);

namespace Storage\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220306134939 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Adds new address fields to facility';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql(
            <<<'SQL'
            ALTER TABLE facilities
                Add street VARCHAR(200) DEFAULT NULL,
                ADD streetnumber VARCHAR(50) DEFAULT NULL,
                ADD zipcode VARCHAR(50) DEFAULT NULL,
                ADD city VARCHAR(200) DEFAULT NULL,
                ADD latitude DOUBLE PRECISION DEFAULT NULL,
                ADD longitude DOUBLE PRECISION DEFAULT NULL
            SQL
        );
    }

    public function down(Schema $schema) : void
    {
    }
}
