<?php

declare(strict_types=1);

namespace Storage\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220306276528 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Cleanup addresses and facilities';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE facilities DROP FOREIGN KEY FK_ADE885D5F5B7AF75');
        $this->addSql('ALTER TABLE facilities DROP address_id');
        $this->addSql('DROP TABLE addresses');
        $this->addSql('DROP INDEX UNIQ_ADE885D5F5B7AF75 ON facilities');
    }

    public function down(Schema $schema) : void
    {
    }
}
