<?php

declare(strict_types=1);

namespace Application\Service;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Storage\Entity\Facility;
use Storage\Entity\TagGroup;
use Storage\Repository\FacilityRepository;
use Storage\Repository\TagGroupRepository;

class SearchServiceFactory
{
    public function __invoke(ContainerInterface $container): SearchService
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);
        /** @var FacilityRepository */
        $facilityRepository = $entityManager->getRepository(Facility::class);
        /** @var TagGroupRepository $tagGroupRepository */
        $tagGroupRepository = $entityManager->getRepository(TagGroup::class);

        return new SearchService(
            $facilityRepository,
            $tagGroupRepository
        );
    }
}