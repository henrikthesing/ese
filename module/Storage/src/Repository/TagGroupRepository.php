<?php

declare(strict_types=1);

namespace Storage\Repository;

use Doctrine\ORM\EntityRepository;
use Storage\Entity\TagGroup;

class TagGroupRepository extends EntityRepository
{
    /**
     * @return TagGroup[]
     */
    public function findByClientId(int $clientId): array
    {
        return $this->findBy([
            'client' => $clientId
        ], [
            'name' => 'ASC'
        ]);
    }

    public function save(TagGroup $tagGroup): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($tagGroup);
        $entityManager->flush($tagGroup);
    }

    public function delete(TagGroup $tagGroup): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->remove($tagGroup);
        $entityManager->flush($tagGroup);
    }
}
