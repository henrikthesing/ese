<?php

namespace Administration\Controller\Console;

use Application\Service\UserService;
use Laminas\Console\Request;
use Laminas\Crypt\Password\Bcrypt;
use Laminas\Mvc\Console\Controller\AbstractConsoleController;

class UserConsoleController extends AbstractConsoleController
{
    private UserService $userService;

    public function __construct(
        UserService $userService
    ) {
        $this->userService = $userService;
    }

    public function updatePasswordAction(): int
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $emailAddress = $request->getParam('email');
        $password = $request->getParam('password');

        if (empty($emailAddress) || empty($password)) {
            return 1;
        }

        $user = $this->userService->findByEmail($emailAddress);
        if ($user === null) {
            return 1;
        }

        $bcrypt = new Bcrypt();
        $passwordHash = $bcrypt->create($password);

        $user->setPassword($passwordHash);
        $this->userService->save($user);

        return 0;
    }

}