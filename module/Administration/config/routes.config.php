<?php

declare(strict_types=1);

namespace Administration;

use Administration\Controller\AdministrationController;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return  [
    'authentication' => require __DIR__ . '/routes/authentication.routes.php',
    'administration' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/admin/[:clientId]',
            'constraints' => [
                'clientId' => '[0-9]*',
            ],
            'defaults' => [
                'controller' => AdministrationController::class,
                'action' => 'index',
            ],
        ],
        'may_terminate' => true,
        'child_routes' => [
            'identity' => require __DIR__ . '/routes/identity.routes.php',
            'clients' => require __DIR__ . '/routes/clients.routes.php',
            'facilities' => require __DIR__ . '/routes/facilities.routes.php',
            'taggroups' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/taggroups',
                    'defaults' => [
                        'action' => 'taggroups',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'add' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/add',
                            'defaults' => [
                                'action' => 'taggroupsAdd',
                            ],
                        ],
                    ],
                    'edit' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/:tagGroupId/edit',
                            'defaults' => [
                                'action' => 'taggroupsEdit',
                            ],
                        ],
                    ],
                    'delete' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/:tagGroupId/delete',
                            'defaults' => [
                                'action' => 'taggroupsDelete',
                            ],
                        ],
                    ],
                    'tags' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/:tagGroupId/tags',
                            'defaults' => [
                                'action' => 'tags',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'add' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/add',
                                    'defaults' => [
                                        'action' => 'tagsAdd',
                                    ],
                                ],
                            ],
                            'edit' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/:tagId/edit',
                                    'defaults' => [
                                        'action' => 'tagsEdit',
                                    ],
                                ],
                            ],
                            'delete' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/:tagId/delete',
                                    'defaults' => [
                                        'action' => 'tagsDelete',
                                    ],
                                ],
                            ],
                        ]
                    ],
                ],
            ],
        ],
    ],
];
