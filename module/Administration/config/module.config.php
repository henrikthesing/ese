<?php

declare(strict_types=1);

namespace Administration;

use Administration\Logger\Logger;
use Administration\Service\AuthAdapter;
use Administration\Service\AuthAdapterFactory;
use Administration\Service\AuthenticationServiceFactory;
use Administration\Service\AuthManagerService;
use Administration\Service\AuthManagerServiceFactory;
use Laminas\Authentication\AuthenticationService;
use Laminas\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => include __DIR__ . '/routes.config.php'
    ],
    'console' => [
        'router' => [
            'routes' => include __DIR__ . '/console.routes.php',
        ]
    ],
    'controllers' => [
        'factories' => [
            Controller\Console\UserConsoleController::class => Controller\Console\UserConsoleControllerFactory::class,

            Controller\AuthenticationController::class => Controller\AuthenticationControllerFactory::class,
            Controller\AdministrationController::class => Controller\AdministrationControllerFactory::class,
            Controller\ClientsController::class => Controller\ClientsControllerFactory::class,
            Controller\FacilitiesController::class => Controller\FacilitiesControllerFactory::class,
            Controller\IdentityController::class => Controller\IdentityControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
        	Logger::class => InvokableFactory::class,
            AuthAdapter::class => AuthAdapterFactory::class,
            AuthManagerService::class => AuthManagerServiceFactory::class,
            AuthenticationService::class => AuthenticationServiceFactory::class,
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'layout/administration' => __DIR__ . '/../view/layout/administration.phtml',
            'layout/login' => __DIR__ . '/../view/layout/login.phtml',
            'administration/administration/index' => __DIR__ . '/../view/administration/index/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
