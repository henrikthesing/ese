<?php

declare(strict_types=1);

namespace Administration\Controller;

use Application\Service\ClientService;
use Application\Service\UserService;
use InvalidArgumentException;
use Laminas\Authentication\AuthenticationService;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\MvcEvent;
use Laminas\Router\RouteMatch;
use Laminas\View\Model\ModelInterface;
use Laminas\View\Model\ViewModel;
use Storage\Entity\Client;
use Storage\Entity\User;
use Storage\Entity\UserRole;

abstract class AbstractAuthenticationController extends AbstractActionController
{
    protected AuthenticationService $authenticationService;

    protected UserService $userService;

    protected ?User $identity = null;

    protected Client $selectedClient;

    protected ClientService $clientService;

    public function __construct(
        AuthenticationService $authenticationService,
        UserService $userService,
        ClientService $clientService
    ) {
        $this->authenticationService = $authenticationService;
        $this->userService = $userService;
        $this->clientService = $clientService;
    }

    public function onDispatch(MvcEvent $e)
    {
        if (!$this->authenticationService->hasIdentity()) {
            return $this->redirect()->toRoute('authentication');
        }

        $this->identity = $this->userService->findByEmail($this->authenticationService->getIdentity());
        if ($this->identity === null) {
            $this->redirect()->toRoute('authentication');
        }

        $layout = $this->layout();
        assert($layout instanceof ModelInterface);

        $this->determineSelectedClient($e->getRouteMatch());
        $layout->setVariable('selectedClient', $this->selectedClient);

        assert($this->identity instanceof User);
        $this->checkUserAccess($this->identity);
        $layout->setVariable('identity', $this->identity);

        assert($this->identity instanceof User);
        $layout->setVariable('clientList', $this->getClientList($this->identity));

        $layout->setTemplate('layout/administration');

        return parent::onDispatch($e);
    }

    protected function createViewModel(array $parameters = []): ViewModel
    {
        return new ViewModel(
            array_merge([
                'client' => $this->selectedClient,
                'identity' => $this->identity,
            ], $parameters)
        );
    }

    private function checkUserAccess(User $identity): void
    {
        if ($identity->getRole()->getId() === UserRole::ROLE_ADMINISTRATOR) {
            // User is administrator
            return;
        }

        if ($this->selectedClient->getId() === $identity->getClient()->getId()) {
            // User is directly connected to the selected client
            return;
        }

        throw new InvalidArgumentException(
            'User has no access to this route',
            187239152847234
        );
    }

    private function determineSelectedClient(?RouteMatch $matchedRoute): void
    {
        if ($matchedRoute === null) {
            throw new InvalidArgumentException(
                'Invalid client Id - route could not be matched',
                1987258723682,
            );
        }
        $client = $this->clientService->find((int) $matchedRoute->getParam('clientId'));
        if ($client === null) {
            throw new InvalidArgumentException(
                'Invalid client Id given',
                1987258723683,
            );
        }

        $this->selectedClient = $client;
    }

    private function getClientList(User $identity): array
    {
        if ($identity->getRole()->getId() === UserRole::ROLE_ADMINISTRATOR) {
            return $this->clientService->findAll();
        }

        return [$this->selectedClient];
    }
}
