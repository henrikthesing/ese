<?php

declare(strict_types=1);

namespace Application\Service;

use Application\Exception\NoClientFoundException;
use Storage\Entity\Client;
use Storage\Repository\ClientRepository;

class ClientService
{
    private ClientRepository $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function find(int $clientId): ?Client
    {
        return $this->clientRepository->find($clientId);
    }

    /**
     * @return Client[]
     */
    public function findAll(): array
    {
        return $this->clientRepository->findAll();
    }

    public function getDefaultClient(): Client
    {
        $client = $this->clientRepository->findOneBy([]);
        if ($client === null) {
            throw new NoClientFoundException(
                'No clients found in database. Please add at least one client to the clients table',
                1634461446706
            );
        }

        return $client;
    }

    public function findAllWithAtLeastOneFacility(): array
    {
        return $this->clientRepository->findAllWithAtLeastOneFacility();
    }

    public function findByClientKey(string $clientKey): ?Client
    {
        return $this->clientRepository->findByClientKey($clientKey);
    }

    public function save(Client $client): void
    {
        $this->clientRepository->save($client);
    }
}
