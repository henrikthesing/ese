<?php

declare(strict_types=1);

namespace Administration;

use Administration\Controller\ClientsController;
use Laminas\Router\Http\Literal;

return [
    'type' => Literal::class,
    'options' => [
        'route' => '/clients',
        'defaults' => [
            'controller' => ClientsController::class,
            'action' => 'index'
        ],
    ],
    'may_terminate' => true,
    'child_routes' => [
        'add' => [
            'type' => Literal::class,
            'options' => [
                'route' => '/add',
                'defaults' => [
                    'action' => 'add',
                ],
            ],
        ],
    ],
];
