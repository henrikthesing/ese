<?php

declare(strict_types=1);

namespace Application\Service;

use Storage\Entity\User;
use Storage\Repository\UserRepository;

class UserService
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function find(int $userId): ?User
    {
        return $this->userRepository->find($userId);
    }

    public function findByEmail(string $email): ?User
    {
        return $this->userRepository->findByEmail($email);
    }

    public function updatePassword(User $user, string $password): bool
    {


        return true;
    }

    public function save(User $user): void
    {
        $this->userRepository->save($user);
    }
}
