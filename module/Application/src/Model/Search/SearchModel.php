<?php

declare(strict_types=1);

namespace Application\Model\Search;

use Application\Model\Search\Struct\Tag;
use Application\Model\Search\Struct\TagGroup;

class SearchModel
{
    private int $clientId;

    private ?string $searchString = null;

    /** @var TagGroup[] */
    private array $tagGroups = [];

    public function __construct(int $clientId)
    {
        $this->clientId = $clientId;
    }

    public function getClientId(): int
    {
        return $this->clientId;
    }

    public function getSearchString(): ?string
    {
        return $this->searchString;
    }

    public function setSearchString(?string $searchString): self
    {
        $this->searchString = $searchString;

        return $this;
    }

    /**
     * @return TagGroup[]
     */
    public function getTagGroups(): array
    {
        return $this->tagGroups;
    }

    /**
     * @param int[] $tagIds
     */
    public function addTagGroupAndTags(int $tagGroupId, array $tagIds): self
    {
        $tags = [];
        foreach ($tagIds as $tagId) {
            $tags[] = new Tag($tagId);
        }

        $this->tagGroups[] = new TagGroup($tagGroupId, $tags);

        return $this;
    }
}
