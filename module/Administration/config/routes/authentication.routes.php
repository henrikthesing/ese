<?php

declare(strict_types=1);

namespace Administration;

use Administration\Controller\AuthenticationController;
use Laminas\Router\Http\Literal;

return [
    'type' => Literal::class,
    'options' => [
        'route' => '/auth',
        'defaults' => [
            'controller' => AuthenticationController::class,
            'action' => 'authenticate'
        ],
    ],
    'may_terminate' => true,
    'child_routes' => [
        'login' => [
            'type' => Literal::class,
            'options' => [
                'route' => '/login',
                'defaults' => [
                    'controller' => AuthenticationController::class,
                    'action' => 'login',
                ],
            ],
        ],
        'logout' => [
            'type' => Literal::class,
            'options' => [
                'route' => '/logout',
                'defaults' => [
                    'controller' => AuthenticationController::class,
                    'action' => 'logout',
                ],
            ],
        ],
    ],
];
