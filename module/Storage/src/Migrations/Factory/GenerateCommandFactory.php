<?php

declare(strict_types=1);

namespace Storage\Migrations\Factory;

use Doctrine\Migrations\Configuration\Configuration;
use Psr\Container\ContainerInterface;
use Storage\Migrations\Command\GenerateCommand;

class GenerateCommandFactory
{
    public function __invoke(ContainerInterface $container): GenerateCommand
    {
        /** @var Configuration $configuration */
        $configuration = $container->get('doctrine.migrations_configuration.orm_default');

        $command = new GenerateCommand();
        $command->setMigrationConfiguration($configuration);

        return $command;
    }
}