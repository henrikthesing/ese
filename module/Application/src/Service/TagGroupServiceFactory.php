<?php

declare(strict_types=1);

namespace Application\Service;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Storage\Entity\Tag;
use Storage\Entity\TagGroup;
use Storage\Repository\TagGroupRepository;
use Storage\Repository\TagRepository;

class TagGroupServiceFactory
{
    public function __invoke(ContainerInterface $container): TagGroupService
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);
        /** @var TagGroupRepository $tagGroupRepository */
        $tagGroupRepository = $entityManager->getRepository(TagGroup::class);
        /** @var TagRepository $tagRepository */
        $tagRepository = $entityManager->getRepository(Tag::class);

        return new TagGroupService(
            $tagGroupRepository,
            $tagRepository
        );
    }
}