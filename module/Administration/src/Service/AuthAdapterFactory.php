<?php

declare(strict_types=1);

namespace Administration\Service;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;

class AuthAdapterFactory
{
    public function __invoke(ContainerInterface $container): AuthAdapter
    {
        return new AuthAdapter(
            $container->get(EntityManager::class)
        );
    }
}
