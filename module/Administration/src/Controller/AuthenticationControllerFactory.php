<?php

declare(strict_types=1);

namespace Administration\Controller;

use Administration\Service\AuthManagerService;
use Application\Service\UserService;
use Laminas\Authentication\AuthenticationService;
use Psr\Container\ContainerInterface;

class AuthenticationControllerFactory
{
    public function __invoke(ContainerInterface $container): AuthenticationController
    {
        return new AuthenticationController(
            $container->get(AuthManagerService::class),
            $container->get(AuthenticationService::class),
            $container->get(UserService::class)
        );
    }
}