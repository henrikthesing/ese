<?php

declare(strict_types=1);

namespace Storage\Migrations\Factory;

use Doctrine\Migrations\Configuration\Configuration;
use Interop\Container\ContainerInterface;
use Storage\Migrations\Command\DiffCommand;

class DiffCommandFactory
{
    public function __invoke(ContainerInterface $container): DiffCommand
    {
        /** @var Configuration $configuration */
        $configuration = $container->get('doctrine.migrations_configuration.orm_default');

        $command = new DiffCommand();
        $command->setMigrationConfiguration($configuration);

        return $command;
    }
}