<?php

declare(strict_types=1);

namespace Storage\Repository;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;
use Storage\Entity\Tag;

class TagRepository extends EntityRepository
{
    public function save(Tag $tag): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($tag);
        $entityManager->flush($tag);
    }

    public function delete(Tag $tag): void
    {
        $sql = 'DELETE FROM facility_tags WHERE tag_id = :tagId';
        $connection = $this->getEntityManager()->getConnection();

        $connection->executeQuery(
            $sql,
            [
                'tagId' => $tag->getId(),
            ],
            [
                'tagId' => Types::INTEGER
            ],
        );
        
        $entityManager = $this->getEntityManager();
        $entityManager->remove($tag);
        $entityManager->flush($tag);
    }
}