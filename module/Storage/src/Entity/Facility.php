<?php

declare(strict_types=1);

namespace Storage\Entity;

use Application\Model\OpeningTimes\OpeningTimesCollection;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="facilities",
 *     options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"}
 * )
 * @ORM\Entity(repositoryClass="Storage\Repository\FacilityRepository")
 */
class Facility
{
    const EXPIRED_AFTER_DAYS = 180;

    /**
     * @ORM\Column(name="id", type="integer", options={"unsigned": true})
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="facilities")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private Client $client;

    /**
     * @ORM\Column(name="name", type="string", length=200)
     */
    private string $name;

    /**
     * @ORM\Column(name="description", type="text", length=60000, nullable=true)
     */
    private ?string $description = null;

    /**
     * @ORM\Column(name="street", type="string", length=200, nullable=true)
     */
    private ?string $street;

    /**
     * @ORM\Column(name="streetnumber", type="string", length=50, nullable=true)
     */
    private ?string $streetNumber;

    /**
     * @ORM\Column(name="zipcode", type="string", length=50, nullable=true)
     */
    private ?string $zipcode;

    /**
     * @ORM\Column(name="city", type="string", length=200, nullable=true)
     */
    private ?string $city;

    /**
     * @ORM\Column(name="latitude", type="float", precision=16, scale=12, length=200, nullable=true)
     */
    private ?float $latitude = null;

    /**
     * @var ?float
     * @ORM\Column(name="longitude", type="float", precision=16, scale=12, length=200, nullable=true)
     */
    private ?float $longitude = null;

    /**
     * @ORM\OneToMany(targetEntity="OpeningTime", mappedBy="facility", cascade={"persist"})
     */
    private Collection $openingTimes;

    /**
     * @ORM\ManyToMany(targetEntity="Tag", cascade={"remove"})
     * @ORM\JoinTable(name="facility_tags",
     *      joinColumns={@ORM\JoinColumn(name="facility_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    private Collection $tags;

    /**
     * @ORM\OneToMany(targetEntity="Contact", mappedBy="facility", cascade={"persist","remove"})
     */
    private Collection $contacts;

    /**
     * @var ?string
     * @ORM\Column(name="phone_number", type="string", length=200, nullable=true)
     */
    private ?string $phoneNumber = null;

    /**
     * @ORM\Column(name="mobile_number", type="string", length=200, nullable=true)
     */
    private ?string $mobileNumber = null;

    /**
     * @ORM\Column(name="fax_number", type="string", length=200, nullable=true)
     */

    private ?string $faxNumber = null;

    /**
     * @ORM\Column(name="website", type="string", length=200, nullable=true)
     */
    private ?string $website = null;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private DateTime $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private ?DateTime $updatedAt;

    public function __construct(
        string $name
    ) {
        $this->name = $name;
        $this->openingTimes = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->createdAt = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getStreetNumber(): ?string
    {
        return $this->streetNumber;
    }

    public function setStreetNumber(string $streetNumber): self
    {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(?float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(?float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getDescription(bool $removeTags = false): ?string
    {
        if ($removeTags && $this->description !== null) {
            return strip_tags($this->description);
        }

        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOpeningTimes(): OpeningTimesCollection
    {
        return new OpeningTimesCollection($this->openingTimes->toArray());
    }

    public function addOpeningTime(OpeningTime $openingTime): self
    {
        $this->openingTimes->add($openingTime);

        return $this;
    }

    public function clearOpeningTimes(): self
    {
        $this->openingTimes->clear();

        return $this;
    }

    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts->add($contact);
        }

        return $this;
    }

    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        $this->tags->add($tag);

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getMobileNumber(): ?string
    {
        return $this->mobileNumber;
    }

    public function setMobileNumber(?string $mobileNumber): self
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    public function getFaxNumber(): ?string
    {
        return $this->faxNumber;
    }

    public function setFaxNumber(?string $faxNumber): self
    {
        $this->faxNumber = $faxNumber;

        return $this;
    }

    public function getWebsite(bool $includePrefix = true): ?string
    {
        return $includePrefix ? 'https://' . $this->website : str_replace('https://', '', $this->website);
    }

    public function setWebsite(string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function hasPhoneNumber(): bool
    {
        return !empty($this->getPhoneNumber());
    }

    public function hasMobileNumber(): bool
    {
        return !empty($this->getMobileNumber());
    }

    public function hasWebsite(): bool
    {
        return !empty($this->getWebsite());
    }

    public function hasFaxNumber(): bool
    {
        return !empty($this->getFaxNumber());
    }

    public function hasDescription(): bool
    {
        return !empty($this->getDescription());
    }

    public function hasExpiredData(): bool
    {
        $comparisonDate = clone $this->createdAt;
        if ($this->updatedAt !== null) {
            $comparisonDate = clone $this->updatedAt;
        }

        $today = new DateTime();
        $distanceInDays = $today->diff($comparisonDate);

        if ($distanceInDays->days && $distanceInDays->days > self::EXPIRED_AFTER_DAYS) {
            return true;
        }

        return false;
    }

    public function getAddressAsString(): string
    {
        $addressParts = array_filter([
            $this->getStreet(),
            $this->getStreetNumber(),
            $this->getZipcode(),
            $this->getCity()
        ]);

        return !empty($addressParts) ? sprintf(
            '%s %s, %s %s',
            $this->getStreet(),
            $this->getStreetNumber(),
            $this->getZipcode(),
            $this->getCity()
        ) : 'Unbekannte Adresse';
    }
}
