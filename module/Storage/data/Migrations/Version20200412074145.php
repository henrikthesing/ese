<?php

declare(strict_types=1);

namespace Storage\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200412074145 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE facility_services (facility_id INT UNSIGNED NOT NULL, service_id INT UNSIGNED NOT NULL, INDEX IDX_253158E6A7014910 (facility_id), UNIQUE INDEX UNIQ_253158E6ED5CA9E6 (service_id), PRIMARY KEY(facility_id, service_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE facility_services ADD CONSTRAINT FK_253158E6A7014910 FOREIGN KEY (facility_id) REFERENCES facilities (id)');
        $this->addSql('ALTER TABLE facility_services ADD CONSTRAINT FK_253158E6ED5CA9E6 FOREIGN KEY (service_id) REFERENCES services (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE facility_services');
    }
}
