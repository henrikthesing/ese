<?php

declare(strict_types=1);

namespace Administration;

use Administration\Controller\AdministrationController;
use Administration\Controller\FacilitiesController;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'type' => Literal::class,
    'options' => [
        'route' => '/facilities',
        'defaults' => [
            'controller' => FacilitiesController::class,
            'action' => 'index',
        ],
    ],
    'may_terminate' => true,
    'child_routes' => [
        'export' => [
            'type' => Literal::class,
            'options' => [
                'route' => '/export',
                'defaults' => [
                    'action' => 'export',
                ],
            ],
        ],
        'edit' => [
            'type' => Segment::class,
            'options' => [
                'route' => '/:facilityId/edit',
                'defaults' => [
                    'action' => 'facilitiesEdit',
                ],
            ],
        ],
        'add' => [
            'type' => Literal::class,
            'options' => [
                'route' => '/add',
                'defaults' => [
                    'action' => 'facilitiesAdd',
                ],
            ],
        ],
        'delete' => [
            'type' => Segment::class,
            'options' => [
                'route' => '/:facilityId/delete',
                'defaults' => [
                    'action' => 'facilitiesDelete',
                ],
            ],
        ],
        'contacts' => [
            'type' => Segment::class,
            'options' => [
                'route' => '/:facilityId/contacts',
                'defaults' => [
                    'controller' => AdministrationController::class,
                    'action' => 'contacts',
                ],
            ],
            'may_terminate' => true,
            'child_routes' => [
                'edit' => [
                    'type' => Segment::class,
                    'options' => [
                        'route' => '/:contactId/edit',
                        'defaults' => [
                            'action' => 'contactsEdit',
                        ],
                    ],
                ],
                'add' => [
                    'type' => Literal::class,
                    'options' => [
                        'route' => '/add',
                        'defaults' => [
                            'action' => 'contactsAdd',
                        ],
                    ],
                ],
                'delete' => [
                    'type' => Segment::class,
                    'options' => [
                        'route' => '/:contactId/delete',
                        'defaults' => [
                            'action' => 'contactsDelete',
                        ],
                    ],
                ],
            ],
        ],
    ],
];
