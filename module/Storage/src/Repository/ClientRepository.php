<?php

declare(strict_types=1);

namespace Storage\Repository;

use Doctrine\ORM\EntityRepository;
use Storage\Entity\Client;

class ClientRepository extends EntityRepository
{
    /**
     * @return Client[]
     */
    public function findAllWithAtLeastOneFacility(): array
    {
        $query = $this->createQueryBuilder('c')
            ->join('c.facilities', 'f')
            ->orderBy('c.name')
            ->getQuery();

        return $query->getResult();
    }

    public function findByClientKey(string $clientKey): ?Client
    {
        $client = $this->findOneBy([
            'clientKey' => $clientKey,
        ]);

        assert($client instanceof Client);

        return $client;
    }

    public function save(Client $client): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($client);
        $entityManager->flush($client);
    }
}

