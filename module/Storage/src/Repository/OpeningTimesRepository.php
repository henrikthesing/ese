<?php

declare(strict_types=1);

namespace Storage\Repository;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;

class OpeningTimesRepository extends EntityRepository
{
    public function removeByFacilityId(int $facilityId): bool
    {
        $connection = $this->getEntityManager()->getConnection();

        $query = <<<'SQL'
            DELETE FROM facility_opening_times WHERE facility_id = :facilityId
        SQL;

        $connection->executeUpdate(
            $query,
            [
                'facilityId' => $facilityId
            ],
            [
                'facilityId' => Types::INTEGER
            ]
        );

        return true;
    }
}