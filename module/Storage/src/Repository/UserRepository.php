<?php

declare(strict_types=1);

namespace Storage\Repository;

use Doctrine\ORM\EntityRepository;
use Storage\Entity\User;

class UserRepository extends EntityRepository
{
    public function findByEmail(string $email): ?User
    {
        /** @var ?User $user */
        $user = $this->findOneBy([
            'email' => $email,
        ]);

        return $user;
    }

    public function save(User $client): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($client);
        $entityManager->flush($client);
    }
}
