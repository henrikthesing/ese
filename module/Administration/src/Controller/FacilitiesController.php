<?php

declare(strict_types=1);

namespace Administration\Controller;

use Application\Service\ClientService;
use Application\Service\FacilityService;
use Application\Service\OpeningTimesService;
use Application\Service\TagGroupService;
use Application\Service\TagService;
use Application\Service\UserService;
use Exception;
use Laminas\Authentication\AuthenticationService;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Storage\Entity\Address;
use Storage\Entity\Facility;
use Storage\Entity\OpeningTime;
use Storage\Entity\TagGroup;

class FacilitiesController extends AbstractAuthenticationController
{
    private FacilityService $facilityService;

    private TagGroupService $tagGroupService;

    private OpeningTimesService $openingTimesService;

    private TagService $tagService;

    public function __construct(
        AuthenticationService $authenticationService,
        UserService $userService,
        ClientService $clientService,
        FacilityService $facilityService,
        TagGroupService $tagGroupService,
        OpeningTimesService $openingTimesService,
        TagService $tagService
    ) {
        parent::__construct($authenticationService, $userService, $clientService);

        $this->facilityService = $facilityService;
        $this->tagGroupService = $tagGroupService;
        $this->openingTimesService = $openingTimesService;
        $this->tagService = $tagService;
    }

    public function indexAction(): ViewModel
    {
        $facilities = $this->facilityService->getFacilities($this->selectedClient);

        return $this->createViewModel([
            'facilities' => $facilities,
        ]);
    }

    /**
     * @return ViewModel|Response
     * @throws Exception
     */
    public function facilitiesEditAction()
    {
        $facilityId = $this->params()->fromRoute('facilityId');

        $facility = $this->facilityService->getFacility((int) $facilityId, $this->selectedClient);
        if ($facility === null) {
            return $this->redirect()->toRoute('administration/facilities', [], [], true);
        }

        $tagGroups = $this->tagGroupService->getTagGroups($this->selectedClient);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $formData = $request->getPost('data');
            $formData['tags'] = $formData['tags'] ?? [];

            $facility->setName($formData['name']);
            $facility->setClient($this->selectedClient);
            $facility->setPhoneNumber($formData['phone']);
            $facility->setMobileNumber($formData['mobile']);
            $facility->setFaxNumber($formData['fax']);
            $facility->setDescription($formData['description']);
            $facility->setWebsite($formData['website']);
            $facility->setStreet($formData['street']);
            $facility->setStreetNumber($formData['streetnumber']);
            $facility->setZipcode($formData['zipcode']);
            $facility->setCity($formData['city']);
            $facility->setLatitude($formData['address']['latitude'] ?? null);
            $facility->setLongitude($formData['address']['longitude'] ?? null);

            $this->openingTimesService->removeOpeningTimes((int) $facilityId);

            foreach ($formData['ot'] as $dayOfWeek => $dayInfo) {
                foreach ($dayInfo as $period => $periodTimes) {
                    if (!empty($periodTimes['from']) && !empty($periodTimes['to'])) {
                        $openingTime = new OpeningTime(
                            $facility,
                            $dayOfWeek,
                            new \DateTime($periodTimes['from'], new \DateTimeZone('Europe/Berlin')),
                            new \DateTime($periodTimes['to'], new \DateTimeZone('Europe/Berlin')),
                            $period
                        );
                        $facility->addOpeningTime($openingTime);
                    }
                }
            }

            foreach ($formData['tags'] as $tagId => $checked) {
                $tag = $this->tagService->getTag($tagId);
                if ((bool) $checked === true) {
                    if (!$facility->getTags()->contains($tag)) {
                        $facility->getTags()->add($tag);
                    }
                } else {
                    $key = $facility->getTags()->indexOf($tag);
                    if ($key) {
                        $facility->getTags()->remove($key);
                    }
                }
            }

            $facility->setUpdatedAt(new \DateTime('now'));
            $this->facilityService->save($facility);

            return $this->redirect()->toRoute('administration', [], [], true);
        }

        return $this->createViewModel([
            'facility' => $facility,
            'tagGroups' => $tagGroups,
        ]);
    }

    public function exportAction()
    {
        $facilities = $this->facilityService->getFacilities($this->selectedClient);
        $filename = sprintf(
            'Einrichtungsliste-%s-%s',
            $this->selectedClient->getName(),
            date('YmdHis')
        );

        $csvHeader = [
            'Id',
            'Name',
            'Straße',
            'Hausnummer',
            'Postleitzahl',
            'Ort',
            'Telefon',
            'Fax',
            'Mobiltelefon',
        ];

        $rows = [];
        foreach ($facilities as $facility) {
            $row = [
                $facility->getId(),
                $facility->getName(),
                $facility->getStreet(),
                $facility->getStreetNumber(),
                $facility->getZipcode(),
                $facility->getCity(),
                $facility->getPhoneNumber(),
                $facility->getFaxNumber(),
                $facility->getMobileNumber(),
            ];

            $rows[] = implode(';', $row);
        }

        $csvContent = implode(';', $csvHeader) .  PHP_EOL;
        $csvContent.= implode(PHP_EOL, $rows);

        header('Content-Type: text/csv;charset=UTF-8');
        header('Content-Encoding: UTF-8');
        header('Content-Disposition: attachment; filename="' . $filename . '.csv"');

        echo $csvContent;
        die;
    }

    /**
     * @return ViewModel|Response
     */
    public function facilitiesAddAction()
    {
        /** @var TagGroup[] $tagGroups */
        $tagGroups = $this->tagGroupService->getTagGroups($this->selectedClient);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $formData = $request->getPost('data');
            $formData['tags'] = $formData['tags'] ?? [];

            $facility = new Facility($formData['name']);
            $facility->setClient($this->selectedClient);
            $facility->setPhoneNumber($formData['phone']);
            $facility->setMobileNumber($formData['mobile']);
            $facility->setFaxNumber($formData['fax']);
            $facility->setDescription($formData['description']);
            $facility->setWebsite($formData['website']);
            $facility->setStreet($formData['street']);
            $facility->setStreetNumber($formData['streetnumber']);
            $facility->setZipcode($formData['zipcode']);
            $facility->setCity($formData['city']);
            $facility->setLatitude($formData['latitude'] ?? 0.00);
            $facility->setLongitude($formData['longitude'] ?? 0.00);

            foreach ($formData['tags'] as $tagId => $checked) {
                $tag = $this->tagService->getTag($tagId);
                if ((bool) $checked === true) {
                    if (!$facility->getTags()->contains($tag)) {
                        $facility->getTags()->add($tag);
                    }
                } else {
                    $key = $facility->getTags()->indexOf($tag);
                    if ($key) {
                        $facility->getTags()->remove($key);
                    }
                }
            }

            foreach ($formData['ot'] as $dayOfWeek => $dayInfo) {
                foreach ($dayInfo as $period => $periodTimes) {
                    if (!empty($periodTimes['from']) && !empty($periodTimes['to'])) {
                        $openingTime = new OpeningTime(
                            $facility,
                            $dayOfWeek,
                            new \DateTime($periodTimes['from'], new \DateTimeZone('Europe/Berlin')),
                            new \DateTime($periodTimes['to'], new \DateTimeZone('Europe/Berlin')),
                            $period
                        );
                        $facility->addOpeningTime($openingTime);
                    }
                }
            }

            $this->facilityService->save($facility);

            return $this->redirect()->toRoute('administration', [], [], true);
        }

        return $this->createViewModel([
            'facility' => $facility ?? null,
            'tagGroups' => $tagGroups,
        ]);
    }

    public function facilitiesDeleteAction(): Response
    {
        $facilityId = $this->params()->fromRoute('facilityId');
        $facility = $this->facilityService->getFacility((int) $facilityId, $this->selectedClient);
        if ($facility === null) {
            return $this->redirect()->toRoute('administration', [], [], true);
        }

        $this->facilityService->delete($facility);

        return $this->redirect()->toRoute('administration', [], [], true);
    }
}
