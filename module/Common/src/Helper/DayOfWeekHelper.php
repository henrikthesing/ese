<?php

declare(strict_types=1);

namespace Common\Helper;

class DayOfWeekHelper
{
    public static function getDayOfWeekString(int $dayOfWeek): string
    {
        switch ($dayOfWeek) {
            case 0:
                return 'Sonntag';
            case 1:
                return 'Montag';
            case 2:
                return 'Dienstag';
            case 3:
                return 'Mittwoch';
            case 4:
                return 'Donnerstag';
            case 5:
                return 'Freitag';
            default:
                return 'Samstag';
        }
    }
}