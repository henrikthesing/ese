<?php

declare(strict_types=1);

namespace Storage\Migrations\Command;

class GenerateCommand extends \Doctrine\Migrations\Tools\Console\Command\GenerateCommand
{
    protected static $defaultName = 'migrations:generate';

    protected function getTemplate(): string
    {
        $template = file_get_contents(__DIR__ . '/MigrationGenerateTemplate.tmpl');

        return $template ?: '';
    }
}