<?php

declare(strict_types=1);

namespace Application\Controller;

use Application\Exception\InvalidClientKeyException;
use Application\Service\ClientService;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\MvcEvent;
use Laminas\View\Model\ModelInterface;
use Storage\Entity\Client;

abstract class AbstractFrontendController extends AbstractActionController
{
    private ClientService $clientService;

    protected ?Client $client;

    public function __construct(ClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    public function onDispatch(MvcEvent $e)
    {
        $clientKey = $this->params()->fromRoute('clientKey', '');
        $this->client = $this->determineClient($clientKey);

        $layout = $this->layout();
        assert($layout instanceof ModelInterface);

        $layout->setVariable('client', $this->client);

        $clients = $this->clientService->findAllWithAtLeastOneFacility();
        $layout->setVariable('clients', $clients);

        return parent::onDispatch($e);
    }

    private function determineClient(string $clientKey): Client
    {
        $client = $this->clientService->findByClientKey($clientKey);
        if ($client === null) {
            throw new InvalidClientKeyException(
                sprintf('Invalid or missing client key "%s"', $clientKey),
                1633124893608
            );
        }

        return $client;
    }
}
