<?php

declare(strict_types=1);

namespace Administration\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\Mvc\MvcEvent;
use Laminas\View\Model\ViewModel;
use Storage\Entity\Client;
use Storage\Entity\User;
use Storage\Entity\UserRole;

class ClientsController extends AbstractAuthenticationController
{
    public function onDispatch(MvcEvent $e)
    {
        parent::onDispatch($e);

        assert($this->identity instanceof User);
        if ($this->identity->getRole()->getId() !== UserRole::ROLE_ADMINISTRATOR) {
            throw new \InvalidArgumentException(
                'User has no access to this route',
                1273681726378
            );
        }
    }

    public function indexAction(): ViewModel
    {
        $clients = $this->clientService->findAll();

        return $this->createViewModel([
            'clients' => $clients,
        ]);
    }

    /**
     * @return ViewModel|Response
     */
    public function addAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $client = new Client();

            $formData = $request->getPost('data');
            $client->setName($formData['name']);
            $client->setClientKey($formData['key']);

            $this->clientService->save($client);

            $this->redirect()->toRoute('administration/clients', [], [], true);
        }

        return $this->createViewModel();
    }

}