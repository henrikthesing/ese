<?php

declare(strict_types=1);

namespace Storage\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211015212919 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Adds userroles';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE userroles (id INT UNSIGNED AUTO_INCREMENT NOT NULL, name VARCHAR(200) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE users ADD userrole_id INT UNSIGNED DEFAULT NULL AFTER id, ADD client_id INT UNSIGNED DEFAULT NULL AFTER userrole_id');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E94A62DE12 FOREIGN KEY (userrole_id) REFERENCES userroles (id)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E919EB6921 FOREIGN KEY (client_id) REFERENCES clients (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E94A62DE12 ON users (userrole_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E919EB6921 ON users (client_id)');

        $this->addSql(<<<'SQL'
            INSERT INTO userroles (id, name) VALUES (1, 'Administrator'), (2, 'User');
        SQL
        );

        $this->addSql(<<<'SQL'
            UPDATE users SET client_id = 1, userrole_id = 1
        SQL
        );
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
