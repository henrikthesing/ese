<?php

declare(strict_types=1);

namespace Application\Service;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Storage\Entity\User;

class UserServiceFactory
{
    public function __invoke(ContainerInterface $container): UserService
    {
        $entityManager = $container->get(EntityManager::class);
        $userRepository = $entityManager->getRepository(User::class);

        return new UserService(
            $userRepository
        );
    }
}
