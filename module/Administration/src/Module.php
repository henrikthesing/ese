<?php

declare(strict_types=1);

namespace Administration;

use Laminas\Console\Adapter\AdapterInterface;

class Module
{
    public function getConfig(): array
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * @param AdapterInterface $console
     * @return array
     */
    public function getConsoleUsage(AdapterInterface $console)
    {
        $result = [];
        foreach ($this->getConfig()['console']['router']['routes'] as $command => $commandOptions) {
            $result[$commandOptions['options']['route']] = '';
        }
        return $result;
    }
}
