<?php

namespace Administration\Controller\Console;

use Application\Service\UserService;
use Psr\Container\ContainerInterface;

class UserConsoleControllerFactory
{
    public function __invoke(ContainerInterface $container): UserConsoleController
    {
        return new UserConsoleController(
            $container->get(UserService::class)
        );
    }
}