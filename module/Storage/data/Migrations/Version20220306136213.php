<?php

declare(strict_types=1);

namespace Storage\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220306136213 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Updates addresses of facilities';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql(
            <<<'SQL'
                UPDATE facilities f
                INNER JOIN addresses a on f.address_id = a.id
                SET f.street = a.street,
                    f.streetnumber = a.streetnumber,
                    f.zipcode = a.zipcode,
                    f.city = a.city,
                    f.latitude = a.latitude,
                    f.longitude = a.longitude;
            SQL
        );
    }

    public function down(Schema $schema) : void
    {
    }
}
