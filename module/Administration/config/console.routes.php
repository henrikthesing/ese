<?php

namespace Administration;

use Administration\Controller\Console\UserConsoleController;

return  [
    'user' => [
        'options' => [
            'route' => 'user update password --email= --password=',
            'defaults' => [
                'controller' => UserConsoleController::class,
                'action' => 'updatePassword'
            ],
        ],
    ],
];