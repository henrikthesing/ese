<?php

declare(strict_types=1);

namespace Application\Service;

use Storage\Entity\Client;
use Storage\Entity\Facility;
use Storage\Repository\FacilityRepository;

class FacilityService
{
    private FacilityRepository $facilityRepository;

    public function __construct(FacilityRepository $facilityRepository)
    {
        $this->facilityRepository = $facilityRepository;
    }

    /**
     * @return Facility[]
     */
    public function getFacilities(Client $client): array
    {
        return $this->facilityRepository->findByClient($client);
    }

    public function getFacility(int $facilityId, Client $client): ?Facility
    {
        return $this->facilityRepository->findOneBy([
            'id' => $facilityId,
            'client' => $client,
        ]);
    }

    public function save(Facility $facility): void
    {
        $this->facilityRepository->save($facility);
    }

    public function delete(Facility $facility): void
    {
        $this->facilityRepository->delete($facility);
    }
}
