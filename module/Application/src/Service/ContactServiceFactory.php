<?php

declare(strict_types=1);

namespace Application\Service;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Storage\Entity\Contact;
use Storage\Repository\ContactRepository;

class ContactServiceFactory
{
    public function __invoke(ContainerInterface $container): ContactService
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);
        /** @var ContactRepository $contactRepository */
        $contactRepository = $entityManager->getRepository(Contact::class);

        return new ContactService(
            $contactRepository
        );
    }

}