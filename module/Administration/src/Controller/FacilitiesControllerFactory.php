<?php

declare(strict_types=1);

namespace Administration\Controller;

use Application\Service\ClientService;
use Application\Service\FacilityService;
use Application\Service\OpeningTimesService;
use Application\Service\TagGroupService;
use Application\Service\TagService;
use Application\Service\UserService;
use Laminas\Authentication\AuthenticationService;
use Psr\Container\ContainerInterface;

class FacilitiesControllerFactory
{
    public function __invoke(ContainerInterface $container): FacilitiesController
    {
        return new FacilitiesController(
            $container->get(AuthenticationService::class),
            $container->get(UserService::class),
            $container->get(ClientService::class),
            $container->get(FacilityService::class),
            $container->get(TagGroupService::class),
            $container->get(OpeningTimesService::class),
            $container->get(TagService::class)
        );
    }
}
