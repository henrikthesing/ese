<?php

declare(strict_types=1);

namespace Administration;

use Administration\Controller\IdentityController;
use Laminas\Router\Http\Literal;

return [
    'type' => Literal::class,
    'options' => [
        'route' => '/identity',
        'defaults' => [
            'controller' => IdentityController::class,
            'action' => 'index',
        ],
    ],
    'may_terminate' => true,
];
